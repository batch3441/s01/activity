import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
       String firstName;
       String lastName;
       Double firstSubject;
       Double secondSubject;
       Double thirdSubject;
    Scanner myObject = new Scanner(System.in)  ;

        System.out.println("First Name: ");
        firstName = myObject.nextLine();

        System.out.println("Last Name: ");
        lastName = myObject.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = myObject.nextDouble();

        System.out.println("Second Subject Grade: ");
        secondSubject = myObject.nextDouble();

        System.out.println("Third Subject Grade: ");
        thirdSubject = myObject.nextDouble();
        double average = (firstSubject +secondSubject + thirdSubject)/3 ;
        System.out.println("Good day," + firstName + " "+ lastName + ".");
        System.out.println( "Your grade average is :" + average );

    }
}